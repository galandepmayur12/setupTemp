package com.demo.site.controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.site.dao.UserDao;
import com.demo.site.model.User;
import com.demo.site.model.UserCredentials;

@RestController
@RequestMapping("/api")
public class UserController {
	public UserDao userDao = new UserDao();
	
	@RequestMapping(value = "/resources/index", method = RequestMethod.GET)
	public String getHome() {
		return "index.html";
	}
	
	@ResponseBody
	@RequestMapping(value="/login", method = RequestMethod.POST)
	public UserCredentials getCredentials(@RequestBody UserCredentials userCredentials) {
		System.out.println("Login" + userCredentials);
		return userCredentials;
	}
	
	@ResponseBody
	@RequestMapping(value = "/user/{emailId}", method = RequestMethod.GET) 
	public User getUser(@PathVariable("emailId") String emailId) {
		System.out.println(emailId);
		System.out.println(userDao.getUser(emailId));
		return userDao.getUser(emailId);		
	}
	@ResponseBody
	@RequestMapping(value = "/users", method = RequestMethod.GET) 
	public ArrayList<User> getUser() {
		System.out.println(userDao.getUsers());
		return userDao.getUsers();		
	}
	@ResponseBody
	@RequestMapping(value = "/user", method = RequestMethod.POST) 
	public User addUser() {
		return userDao.createUser(new User("dsa", "Dsa", "dsagf", "fds@fdfjdk", new Date(System.currentTimeMillis())));		
	}
}
