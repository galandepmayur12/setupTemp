package com.demo.site.model;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api")
public class UserController {
	
	@RequestMapping(value="/resources")
	public String home() {
		return "index.html";
	}

}
